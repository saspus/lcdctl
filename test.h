#pragma once


void test_colors(void);
void test_chargen_internal(void);
void test_chargen_external(void);
void test_chargen_external_ascii(void);
void test_text_scrolling(void);
void test_bunchoellipses(void);
