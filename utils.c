#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/types.h>
#include "utils.h"
#include <time.h>

void sleep_ms (unsigned int ms)
{
	struct timespec sleeper, dummy ;

	sleeper.tv_sec  = (time_t)(ms / 1000) ;
	sleeper.tv_nsec = (long)(ms % 1000) * 1000000 ;

	nanosleep (&sleeper, &dummy) ;
}

void pabort(const char *s)
{
	perror(s);
	abort();
}
