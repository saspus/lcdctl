CC=gcc  
CFLAGS=-Wall -Wmissing-prototypes -Wmissing-declarations -Werror -D_GNU_SOURCE $(DEBUG) -std=c99

## CUSTOMIZATIONS
# List of dependencies
DEPS = lcdctl.h spi.h lcd.h utils.h test.h  lcd_regs.h colors.h log.h spectrum.h kiss_fftr.h kiss_fft.h copper.h summer.h jet.h winter.h
# Lisst of object files
OBJ = lcdctl.o spi.o lcd.o utils.o test.o colors.o log.o spectrum.o kiss_fftr.c kiss_fft.c
# Product name
PROD = lcdctl



## GENERIC SHITE

# Build each object file form a first file $< from the right sice (which is .c) and dependencies
%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $< 

# Link the main executable from everything on the right $^ 
$(PROD): $(OBJ)
	$(CC) $(CFLAGS) -lm -o $@ $^ 


run: $(PROD)
	./$(PROD) --speed 2000000

.PHONY: clean
clean:
	rm -f $(PROD) *.o


debug: 
	$(MAKE) $(MAKEFILE) DEBUG="-g -DDEBUG"	
#	$(MAKE) $(MAKEFILE) DEBUG="-g -DDEBUG -D SPI_LOG_VERBOSE"	

release:
	$(MAKE) $(MAKEFILE) 
