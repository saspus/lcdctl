
/* 
 * LCDCTL is a LCD control utility.
 *
 *
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <linux/spi/spidev.h>
#include "utils.h"
#include "spi.h"
#include "lcd.h"
#include "test.h"
#include <time.h>
#include "log.h"
#include "spectrum.h"

// settings
// TODO: Make a structure

static const char *device = "/dev/spidev0.0";
static uint8_t mode;
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay;


static void print_usage(const char *prog)
{
	LOG("Usage: %s [-DsbdlHOLC3]\n", prog);
	LOG("  -D --device   device to use (default /dev/spidev1.1)\n"
	     "  -s --speed    max speed (Hz)\n"
	     "  -d --delay    delay (usec)\n"
	     "  -b --bpw      bits per word \n"
	     "  -l --loop     loopback\n"
	     "  -H --cpha     clock phase\n"
	     "  -O --cpol     clock polarity\n"
	     "  -L --lsb      least significant bit first\n"
	     "  -C --cs-high  chip select active high\n"
	     "  -3 --3wire    SI/SO signals shared\n"
	     "  -g --loglevel set log level (0-7). default - 3\n"
	     );
	exit(1);
}

static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "device",  1, 0, 'D' },
			{ "speed",   1, 0, 's' },
			{ "delay",   1, 0, 'd' },
			{ "bpw",     1, 0, 'b' },
			{ "loop",    0, 0, 'l' },
			{ "cpha",    0, 0, 'H' },
			{ "cpol",    0, 0, 'O' },
			{ "lsb",     0, 0, 'L' },
			{ "cs-high", 0, 0, 'C' },
			{ "3wire",   0, 0, '3' },
			{ "no-cs",   0, 0, 'N' },
			{ "ready",   0, 0, 'R' },
			{ "loglevel", 0, 0, 'g' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "D:s:d:b:lHOLC3NRg:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'D':
			device = optarg;
			break;
		case 's':
			speed = atoi(optarg);
			break;
		case 'd':
			delay = atoi(optarg);
			break;
		case 'b':
			bits = atoi(optarg);
			break;
		case 'l':
			mode |= SPI_LOOP;
			break;
		case 'H':
			mode |= SPI_CPHA;
			break;
		case 'O':
			mode |= SPI_CPOL;
			break;
		case 'L':
			mode |= SPI_LSB_FIRST;
			break;
		case 'C':
			mode |= SPI_CS_HIGH;
			break;
		case '3':
			mode |= SPI_3WIRE;
			break;
		case 'N':
			mode |= SPI_NO_CS;
			break;
		case 'R':
			mode |= SPI_READY;
			break;
		case 'g':
			set_log_level(atoi(optarg));
			break;
		default:
			print_usage(argv[0]);
			break;
		}
	}
}

int main(int argc, char *argv[])
{

	parse_opts(argc, argv);
	unsigned long fd = spi_open(device, &mode, &bits, &speed, &delay);

        srand(time(0));

	lcd_init(fd);
	lcd_display_on(true);
	lcd_bklight_on(true);
	lcd_bklight_level(0x8f);


	LOG("Register 0 = 0x%x\n", lcd_reg_read(0));
	sleep_ms(500);	

	test_colors();
	uint32_t loopindex=0;
	while (1){
		printf("--- Loop %04d ---\n", loopindex++);
		test_spectrum();
		// test_colors();
		test_bunchoellipses();
		test_chargen_internal();
		test_chargen_external_ascii();
		test_text_scrolling();
		//	      Test_CharGen(); 
		//
		//
		//	      
		sleep_ms(2000);
	}
	spi_close(fd);

	return 0;
}
