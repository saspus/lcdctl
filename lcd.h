#pragma once
#include <linux/types.h>
#include <stdint.h>
#include <stdbool.h>

//TODO sort this out
uint8_t lcd_status_read(void);
void lcd_wait_idle(void);
void lcd_wait_bte_idle(void);
void lcd_wait_dma_idle(void);

void lcd_bulk_write(uint8_t*data, uint32_t count);
// Private interface
uint8_t lcd_reg_read(uint8_t regnum);
void lcd_reg_write(uint8_t regnum, uint8_t data);
void lcd_cmd_write(uint8_t cmd);
void lcd_dat_write(uint8_t data);
uint8_t lcd_dat_read(void);     
void lcd_pll_init(void);

bool lcd_poll_wait(uint8_t reg, uint8_t bits);

// Public Interface
void lcd_init(unsigned long spi_device);
void lcd_display_on(bool on);
void lcd_bklight_on(bool on);
void lcd_bklight_level(uint8_t v);

void lcd_text_bk_color1(uint16_t b_color);
void lcd_text_bk_color(uint8_t setR, uint8_t setG, uint8_t setB);
void lcd_text_fg_color1(uint16_t b_color);
void lcd_text_fg_color(uint8_t setR, uint8_t setG, uint8_t setB);

void lcd_clear(void);
bool lcd_clear_barrier(void);

void lcd_mode_txt(void);
void lcd_mode_gfx(void);
void lcd_text_props(uint8_t scale, bool transparent);
void lcd_font_write_pos(uint16_t X,uint16_t Y);
void lcd_text_out(const char *str);


void lcd_scroll_window(uint16_t HS, uint16_t VS, uint16_t HE, uint16_t VE);
void lcd_scroll_offset(uint16_t X, uint16_t Y);

void lcd_active_window(uint16_t HS, uint16_t VS, uint16_t HE, uint16_t VE);
void lcd_ellipse(int16_t xCenter, int16_t yCenter, int16_t longAxis, int16_t shortAxis, uint16_t color, bool filled) ;
void lcd_triangle( int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color, bool filled);
void lcd_rectangle( int16_t x, int16_t y, int16_t w, int16_t h, int16_t color, bool filled);
void  lcd_curve_corner(int16_t xc, int16_t yc, int16_t longaxis, int16_t shortaxis, uint8_t icorner, uint16_t color, bool filled);
