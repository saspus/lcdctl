
#include <linux/types.h>
#include <sys/time.h>
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "lcd.h"
#include "spectrum.h"
#include "utils.h"
#include "colors.h"
#include "lcd_regs.h"
#include "solarized.h"
#include "log.h"
#include "kiss_fftr.h"






void test_spectrum()
{

	uint16_t n_bins=800;
	float f_max=5000;
	float f_sampling=2*f_max;
// float t_sample_interval=1.0/f_sampling;
        float pi = M_PI;

	// prepare fft
	uint16_t n_fft=2*n_bins; 

	float hanning[n_fft];

	for (int i = 0; i < n_fft; ++i)
		hanning[i]=0.5*(1 - cos(2*pi*i/(n_fft-1)));
        
	uint16_t n_samples = 3*n_bins;
	
	float samples[n_samples];

	// Fill samples with test data. This will be done on time in the actual implementation. We keep three blocks of data. First two we use to copy to working window. The third one we fill with new data. Then repeat
	// 1,2-> fft; 3<-data
	// 2,3-> fft; 1<-data
	// 3,1-> fft, 2<-data

	//TODO loop entire thing with slowly varying amplitudes to chagiger the spectrum
	// for now just fill it completely
	for (int i = 0; i < sizeof(samples)/sizeof(samples[0]); ++i){
		float tt = 1.0 / f_sampling * i; 
		samples[i]=1.0*sin(2*pi*100.*tt)+1.7*sin(2*pi*215.*tt)+2.2*sin(2*pi*873.*tt)+0.6*sin(2*pi*2143.*tt)+1.91*sin(2*pi*4462.*tt)+2.9*sin(2*pi*3349.*tt)+sin(2*pi*3000.*tt);
	}


        // Doing single iteration of the processing now.
	// 1. Copy the relevant block into the working copy from samples starting from the index i_sample_start
	// also applly hanning window function inplace
	uint16_t i_sample_start=0;
	float ff[n_fft];
	kiss_fft_cpx ss[n_fft];
	for (int i = 0; i < n_fft; ++i) {
	       ff[i] = samples[(i_sample_start+i) % n_samples] * hanning[i];
//	       printf("%g\n", ff[i].r);
	}
	
	kiss_fftr_cfg cfg= kiss_fftr_alloc(n_fft, 0, 0, 0);
	kiss_fftr(cfg, ff, ss);
	free(cfg);



	float max = 0;
	for (int i = 0; i < n_fft/2; ++i)
	{
		ss[i].r= sqrt(ss[i].r * ss[i].r +  ss[i].i * ss[i].i);
		if (ss[i].r > max)
			max = ss[i].r;
	}


       for (int i = 0; i < n_fft/2; ++i)
	{
		ss[i].r=20*log(ss[i].r/max);
	   //     printf("%g\n", ss[i].r);

		int n = (int) (ss[i].r);
		if (n < -100)
			n = -100;
		printf("%.*s\n", 100+n,
				"****************************************************************************************************");

	}
      
	for (int i = 0; i < n_fft/2; ++i)
	{
		printf("%g\t %g\n", i*1.0,  ss[i].r);

	}
	//exit(1);
}



