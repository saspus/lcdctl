
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "spi.h"
#include "utils.h"
#include "log.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
struct spi_device_t {
	int fd;
	struct spi_ioc_transfer tr;
};

unsigned long	spi_open(const char *device, uint8_t *mode, uint8_t *bits, uint32_t *speed, uint16_t *delay)
{
	struct spi_device_t * d = calloc(1, sizeof(struct spi_device_t));

        d->fd = open(device, O_RDWR);
	if (d->fd < 0)
		pabort("can't open device");

	/*
	 * spi mode
	 */
	int ret = ioctl(d->fd, SPI_IOC_WR_MODE, mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(d->fd, SPI_IOC_RD_MODE, mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(d->fd, SPI_IOC_WR_BITS_PER_WORD, bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(d->fd, SPI_IOC_RD_BITS_PER_WORD, bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(d->fd, SPI_IOC_WR_MAX_SPEED_HZ, speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(d->fd, SPI_IOC_RD_MAX_SPEED_HZ, speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	LOG("spi mode: %d\n", *mode);
	LOG("bits per word: %d\n", *bits);
	LOG("max speed: %d Hz (%d KHz)\n", *speed, *speed/1000);

        d->tr.delay_usecs = *delay;
        d->tr.speed_hz = *speed;
        d->tr.bits_per_word = *bits;

	return (unsigned long) d;

}
void 	spi_close(unsigned long device)
{
	close(((struct spi_device_t*)device)->fd);
}

uint8_t spi_tr(unsigned long d, uint8_t v0, uint8_t v1)
{
	uint8_t rx[2] = {0, };
	uint8_t tx[2] = {v0, v1};
	
	struct spi_device_t * device = (struct spi_device_t*) d;
#ifdef SPI_LOG_VERBOSE
	INFO("--> ");
	int i;
	for (i = 0; i < ARRAY_SIZE(tx); i++)
		INFO("%.2X ", tx[i]);
	INFO("\t");
#endif
	device->tr.tx_buf = (unsigned long)tx;
	device->tr.rx_buf = (unsigned long)rx;
	device->tr.len = ARRAY_SIZE(tx);
	
	int ret = ioctl(device->fd, SPI_IOC_MESSAGE(1), &device->tr);
	if (ret < 1)
		pabort("can't send spi message");

#ifdef SPI_LOG_VERBOSE
	INFO("<-- ");
	for (i = 0; i < ARRAY_SIZE(rx); i++) 
		INFO("%.2X ", rx[i]);
	INFO("\n");
#endif
        return rx[1];
}

uint8_t spi_tr_bulk(unsigned long d, uint8_t *data, uint32_t count)
{
	
	struct spi_device_t * device = (struct spi_device_t*) d;
	
	device->tr.tx_buf = (unsigned long)data;
	device->tr.rx_buf = (unsigned long)data;
	device->tr.len = count;
	
	int ret = ioctl(device->fd, SPI_IOC_MESSAGE(1), &device->tr);
	
	if (ret < 1)
		pabort("can't send spi message");

        return data[0];
}
