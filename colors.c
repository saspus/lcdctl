#include <stdlib.h>
#include <math.h>
#include "colors.h"
#include <stdio.h>

// https://gist.github.com/fairlight1337/4935ae72bcbcc1ba5c72
/*! \brief Convert HSV to RGB color space

  Converts a given set of HSV values `h', `s', `v' into RGB
  coordinates. The output RGB values are in the range [0, 1], and
  the input HSV values are in the ranges h = [0, 360], and s, v =
  [0, 1], respectively.

  \param fR Red component, used as output, range: [0, 1]
  \param fG Green component, used as output, range: [0, 1]
  \param fB Blue component, used as output, range: [0, 1]
  \param fH Hue component, used as input, range: [0, 360]
  \param fS Hue component, used as input, range: [0, 1]
  \param fV Hue component, used as input, range: [0, 1]

*/


uint16_t HEXto565(uint32_t x)
{
 	return RGB888to565(x & 0xFF, (x >> 8)&0xFF, (x >>16)&0xFF);
}

uint16_t HSVtoRGB565(float fH, float fS, float fV) {
	float fC = fV * fS; // Chroma
	float fHPrime = fmod(fH / 60.0, 6);
	float fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
	float fM = fV - fC;
        
	float fR, fG, fB;
	if(0 <= fHPrime && fHPrime < 1) {
		fR = fC;
		fG = fX;
		fB = 0;
	} else if(1 <= fHPrime && fHPrime < 2) {
		fR = fX;
		fG = fC;
		fB = 0;
	} else if(2 <= fHPrime && fHPrime < 3) {
		fR = 0;
		fG = fC;
		fB = fX;
	} else if(3 <= fHPrime && fHPrime < 4) {
		fR = 0;
		fG = fX;
		fB = fC;
	} else if(4 <= fHPrime && fHPrime < 5) {
		fR = fX;
		fG = 0;
		fB = fC;
	} else if(5 <= fHPrime && fHPrime < 6) {
		fR = fC;
		fG = 0;
		fB = fX;
	} else {
		fR = 0;
		fG = 0;
		fB = 0;
	}

	fR += fM;
	fG += fM;
	fB += fM;

	//printf("%g\t%g\t%g\n", fR, fG, fB); i

	uint16_t R = (uint16_t) (fR * 31 + 0.499); if (R>31) R=31;
	uint16_t G = (uint16_t) (fG * 63 + 0.499); if (G>63) G=63;
	uint16_t B = (uint16_t) (fB * 31 + 0.499); if (B>31) B=31;

       // if (fR > 1.0) fR=1.0;
      //  if (fG > 1.0) fG=1.0;31
      //  if (fB > 1.0) fB=1.0;

        return B + (G << 5) + (R << 11); 

	return RGB888to565(fR*255, fG*255, fB*255);
}


