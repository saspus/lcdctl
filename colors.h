#pragma once
#ifndef COLORS_H
#define COLORS_H

#include <stdint.h>

#define	RGB888to565(r,g,b) ((((uint32_t)(r) >> 3) << (6+5)) | (((uint32_t)(g) >> 2) << (5)) | ((uint32_t)(b) >> 3))

uint16_t HEXto565(uint32_t x);

//uint16_t RGB565(uint8_t r, uint8_t g, uint8_t b);
uint16_t HSVtoRGB565(float fH, float fS, float fV);

#define color_brown   0x40c0
#define color_black   0x0000
#define color_white   0xffff
#define color_red     0xf800
#define color_green   0x07e0
#define color_blue    0x001f
#define color_yellow  color_red|color_green
#define color_cyan    color_green|color_blue
#define color_purple  color_red|color_blue

#endif
