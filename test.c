#include <linux/types.h>
#include <sys/time.h>
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "lcd.h"
#include "test.h"
#include "utils.h"
#include "colors.h"
#include "lcd_regs.h"
#include "solarized.h"
#include "log.h"
#include "jet.h"
#include "copper.h"
#include "summer.h"
#include "winter.h"


uint16_t random_color(void);
uint16_t random_color()
{
	return HSVtoRGB565(360.0f * rand()/RAND_MAX, 0.5, 1.0);
}
void test_colors(void)
{

        int n_colors=800;
        int x_band=800/n_colors;

        const uint16_t const *colormaps[]={colormap_copper, colormap_jet, colormap_summer, colormap_winter};


	LOG("Test Display Clear\n");
        int N = 5;
	int i = N;

	// test writing directry to memory
        
        uint16_t sp_bottom = 350;
	lcd_reg_write(0x46, 0& 0xFF);
	lcd_reg_write(0x47, 0>>8);
	lcd_reg_write(0x48, (sp_bottom -1) & 0xFF);
	lcd_reg_write(0x49, (sp_bottom -1) >>8);

	
        int width = 800;
	int height = 480;
	uint8_t *ppp = malloc(2*width+1);
	uint32_t plain = 0;
	
	lcd_active_window(0, 0, width-1, sp_bottom-1);
	lcd_scroll_window(0, 0, width-1, sp_bottom-1);

	int n_overscrolls = 20;
        for (int ii = 0; ii < n_overscrolls*sp_bottom; ++ii)
        {
		uint8_t *p0=ppp;
		*p0++=0;

		for (int pp = 0; pp < width; ++pp){

                uint16_t c =  HSVtoRGB565(2.17*360.0*(plain++)/(sp_bottom*800), 1.0, 0.5+0.5*sin(5*3.141*ii/n_overscrolls/sp_bottom));
		     *p0++= (c >> 8) & 0xFF;
		     *p0++= (c >> 0) & 0xFF;
		}
    	    



		lcd_cmd_write(0x2);
		lcd_bulk_write(ppp, 2*width+1);
		lcd_scroll_offset(0,ii%sp_bottom);
	}
	free(ppp);
	
	sleep_ms(2000);	
	
	lcd_active_window(0, 0, width-1,height -1);
	while (--i){

		for(int cm = 0; cm < sizeof(colormaps)/sizeof(colormaps[0]); ++cm)
		{

			lcd_text_bk_color1(HSVtoRGB565(360.0f*i/N, 0.5, 1.0));
			lcd_clear();
			lcd_clear_barrier();

			sleep_ms(500);	
			int inc = +1;
			int v = 0;
	        	for (int c = 0; c < n_colors; ++c){

				lcd_rectangle(c*x_band,0, (c+1)*x_band, 479,  
						//HSVtoRGB565(360.0f * c/n_colors, s, vm + (vx-vm)*c/n_colors),
						colormaps[cm][v],
						true);

				if (v == 254 && inc > 0)
					inc = -1;
				
				if (v == 0 && inc < 0)
					inc = 1;

                                v+=inc;


			}
			sleep_ms(2000);
		}


	        	for (int c = 0; c < n_colors; ++c){

				float vm = 0.0;
				float vx =1;
				float s = 1;
				lcd_rectangle(c*x_band,0, (c+1)*x_band, 479,  
						HSVtoRGB565(240-240.0f * c/n_colors, s, vm + (vx-vm)*c/n_colors),
						true);


			}

			sleep_ms(4000);

	}


	/*lcd_text_bk_color1(color_black);
	  lcd_clear();
	  sleep_ms(700);
	  puts("display red");
	  lcd_text_bk_color1(color_red);
	  lcd_clear();
	  sleep_ms(700);
	  puts("display green");
	  lcd_text_bk_color1(color_green);
	  lcd_clear();
	  sleep_ms(700);
	  puts("display blue");
	  lcd_text_bk_color1(color_blue);
	  lcd_clear();
	  sleep_ms(700);
	  puts("display white");
	  lcd_text_bk_color1(color_white);
	  lcd_clear();
	  sleep_ms(700);
	  puts("display cyan");
	  lcd_text_bk_color1(color_cyan);
	  lcd_clear();
	  sleep_ms(700);
	  puts("display yello");
	  lcd_text_bk_color1(color_yellow);
	  lcd_clear();
	  sleep_ms(700);
	  lcd_text_bk_color1(color_purple);
	  lcd_clear();
	  sleep_ms(700);
	  */
}

void test_chargen_internal(void)
{
	LOG("Testing Internal\n");
	
	lcd_text_bk_color1(RGB888to565(0,0,0 ));

//        lcd_text_bk_color1(HEXto565(base03));
	lcd_clear();
	lcd_clear_barrier();
	
	lcd_mode_txt();
	lcd_reg_write(RA8875_FNCR0, RA8875_FNCR0_CGROM_INTERNAL | RA8875_FNCR0_ISO8859_1);
	lcd_reg_write(RA8875_FWTSR, RA8875_FWTSR_16 | 0x01);	//Set the characters mode 16 x16 / spacing 1
	
	lcd_text_props(0, false);
	lcd_font_write_pos(10,5);//Text written to the position
      //  lcd_text_fg_color1(color_white);
	lcd_text_out("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG. INTERNAL.");
	lcd_font_write_pos(10,16+5);//Text written to the position
       	lcd_text_fg_color1(random_color());
      //  lcd_text_fg_color1(color_green);
	lcd_text_out("The quick brown fox jumps over the lazy dog. Internal.");
	
	lcd_text_props(1, false);
	lcd_font_write_pos(10,16+35);//Text written to the position
	lcd_text_fg_color1(random_color());//lcd_text_fg_color1(color_red);
	lcd_text_out("This is ");
	
	lcd_text_fg_color1(random_color());//lcd_text_fg_color1(color_white);
	lcd_text_props(2, false);
	lcd_text_out("another ");
	
	lcd_text_props(3, false);
	lcd_text_fg_color1(random_color());//lcd_text_fg_color1(color_blue);
	lcd_text_out("English ");
	
	                        
    //    sleep_ms(2000);
}

void test_chargen_external_ascii(void){
	LOG("Testing External\n");

        lcd_text_fg_color1(color_green);			//Set the foreground color
        
	lcd_mode_txt();

	lcd_reg_write(RA8875_FNCR0, RA8875_FNCR0_CGROM_EXTERNAL|RA8875_FNCR0_ISO8859_1);	// External charset
	lcd_reg_write(RA8875_SFCLR, RA8875_SFCLR_SYSCKJ_4);   	// SYSCLK/4
	lcd_reg_write(RA8875_FWTSR, RA8875_FWTSR_16 | 1);     	// Char size and inter-char space
	lcd_reg_write(0x05,0x28);	// The waveform 3   2 byte dummy Cycle) //TODO figure this out 
	lcd_reg_write(RA8875_FNCR1, RA8875_FNCR1_FULL_ALIGNMENT| 0 ); // no zoom
	lcd_reg_write(RA8875_FLDR, 5); 	// FOnt line distance setting

	lcd_text_props(0, false);
	
	uint16_t y = 16+88;
	uint16_t h = 16 + 5;
	
	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_ASCII | RA8875_SFRS_STYLE_0);
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_fg_color1(random_color()); lcd_text_out("The quick brown fox jumps over the lazy dog. External. ASCII. Normal");
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_fg_color1(random_color()); lcd_text_out("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG. EXTERNAL. ASCII. NORMAL");

	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_ASCII | RA8875_SFRS_STYLE_1);
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_fg_color1(random_color()); lcd_text_out("The quick brown fox jumps over the lazy dog. External. ASCII. Arial");
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_fg_color1(random_color()); lcd_text_out("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG. EXTERNAL. ASCII. ARIAL");


	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_ASCII | RA8875_SFRS_STYLE_2);
        lcd_text_fg_color1(random_color()); lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_out("The quick brown fox jumps over the lazy dog. External. ASCII. Roman");
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_fg_color1(random_color()); lcd_text_out("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG. EXTERNAL. ASCII. ROMAN");


//	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_ASCII | RA8875_SFRS_STYLE_3);
//        lcd_font_write_pos(10, y += h);//Text written to the position
//	lcd_text_out("The quick brown fox jumps over the lazy dog. External. ASCII. Bold");


/*       
	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_CYRILLIC | RA8875_SFRS_STYLE_0);
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_out("Жопень. The quick brown fox jumps over the lazy dog. External. Cyrillic. Normal");

	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_CYRILLIC | RA8875_SFRS_STYLE_1);
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_out("Жопень. The quick brown fox jumps over the lazy dog. External. Cyrillic. Arial");


	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_CYRILLIC | RA8875_SFRS_STYLE_2);
        lcd_font_write_pos(10, y += h);//Text written to the position
	lcd_text_out("Жопень. The quick brown fox jumps over the lazy dog. External. Cyrillic. Roman");


//	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_CYRILLIC | RA8875_SFRS_STYLE_3);
//        lcd_font_write_pos(10, y += h);//Text written to the position
//	lcd_text_out("The quick brown fox jumps over the lazy dog. External. Cyrillic. Bold");

*/
	
	lcd_reg_write(RA8875_SFRS, RA8875_SFRS_ROM_GT30L16U2W |  RA8875_SFRS_CODING_ASCII | RA8875_SFRS_STYLE_0);
	

	lcd_text_fg_color1(random_color());//lcd_text_fg_color1(color_white);
	lcd_text_props(1, false);
	lcd_font_write_pos(10, y+=h);//Text written to the position
	lcd_text_fg_color1(random_color());//lcd_text_fg_color1(color_red);
	lcd_text_out("This is ");
       
	lcd_text_fg_color1(random_color());//lcd_text_fg_color1(color_white);
	lcd_text_props(2, false);
	lcd_text_out("another ");
	
	lcd_text_props(3, false);
	lcd_text_fg_color1(random_color());//lcd_text_fg_color1(color_blue);
	lcd_text_out("English ");
	

	sleep_ms(2000);
}

void test_text_scrolling()
{
	// Add buncho text
	// Switch to internal text gen because external sucks.
	//
	//

	lcd_reg_write(RA8875_FNCR0, RA8875_FNCR0_CGROM_INTERNAL | RA8875_FNCR0_ISO8859_1);
	lcd_reg_write(RA8875_FWTSR, RA8875_FWTSR_16 | 0x01);	//Set the characters mode 16 x16 / spacing 1

	lcd_text_props(0, false);
//	lcd_text_fg_color1(RGB888to565(200,200,200));
	lcd_text_fg_color1(color_white);
	lcd_font_write_pos(10, 340);
	int s = 12;
	while (--s)  
	{
		lcd_text_fg_color1(random_color());
		lcd_text_out("The quick brown fox jumps over the lazy dog. ");
	}
	sleep_ms(2000);
	// lcd_scroll_offset Out
	//
	LOG("Testing Scrolling\n");
	// 800x480 in 16 bit color mode only 1 layer is supported. So we can't do two full window scroll
	// But I guess we don't have to/ We'll just erase the stuff at the top then scroll 
	// and then add stuff at the bottom. 
	//
	// lcd_scroll_window(0,0, 800, 480);
    	// lcd_reg_write(RA8875_LTPR0, RA8875_LTPR0_BUFFER_SCROLL);
                                
	lcd_active_window(0, 0, 799, 479);
	lcd_scroll_window(0, 5+16+1, 799, 479);
	int16_t p = 2; 
	int16_t i = 0;
	while (--p)
	{
		while(i++ < 480 - 5-16 -1 -1){ sleep_ms(10); lcd_scroll_offset(0, i);} 
		while(i-- >   0){ sleep_ms(10); lcd_scroll_offset(0, i);}       
	}
 //       while(i++ < 800){ sleep_ms(10); lcd_scroll_offset(i, 0);} 
 //       while(i-- >   0){ sleep_ms(10); lcd_scroll_offset(i, 0);}       

}


void test_chargen_external(void)
{
	LOG("Testing External\n");
        lcd_text_fg_color1(color_white);//Set the foreground color
	lcd_text_bk_color1(color_black);//Set the background color		
	lcd_clear();//Start screen clearing (display window)
	lcd_clear_barrier();

	lcd_reg_write(0x21,0x20);//Select the external character
	lcd_reg_write(0x06,0x03);//Set the frequency
	lcd_reg_write(0x2E,0x00);
	lcd_reg_write(0x2F,0x2C);//Serial Font ROM Setting GT30L16U2W unicode
	lcd_reg_write(0x05,0x28);// The waveform 3   2 byte dummy Cycle) 
	lcd_reg_write(0x22,0x80);//Full alignment is enable.The text background color . Text don't rotation. 0x zoom		
lcd_reg_write(0x29,0x05);//Font Line Distance Setting

	lcd_font_write_pos(208,45);//Text written to the position
	lcd_reg_write(0x40,0x80);//Set the character mode
	lcd_cmd_write(0x02);//start write data
	lcd_text_out("ÉîÛÚÐñÈÕ¶«·½¿Æ¼¼ÓÐÏÞ¹«Ë¾");

	lcd_text_fg_color1(color_red);//Set the foreground color
	lcd_reg_write(0x2E,0x01);//Set the characters mode 16 x16 / spacing 1
	lcd_font_write_pos(100,90);//Text written to the position
	lcd_text_out("TEL:755-33503874 FAX:755-33507642");
	lcd_font_write_pos(100,120);//Text written to the position
	lcd_text_out("WWW.BUY-DISPLAY.COM");
	lcd_font_write_pos(100,150);//Text written to the position
	lcd_text_out("E-mail:mrket@lcd-china.com");
	lcd_font_write_pos(100,180);//Text written to the position
	lcd_text_out("AD:Room 6G,Building A1,Zhujiang Square,Zhongxin Cheng,Longgang District,                      ShenZhen,China.");
	lcd_reg_write(0x29,0x00);//Font Line Distance Setting
	lcd_reg_write(0x22,0x05);//Full alignment is disable.The text background color . Text don't rotation. 2x zoom		
	lcd_text_fg_color1(color_green);//Set the foreground color
	lcd_reg_write(0x2E,0x00);//Set the characters mode 16 x16 / spacing 0
	lcd_font_write_pos(0x00,250);//Text written to the position
	lcd_text_out("ER-TFTM050-3£¬Optional Chinese / English character library,  MicroSD cord,Falsh.Font Support 2/3/4 times zoom."
			"     Support8/16-bit 8080/6800 Series bus,Support serial 3/4wire SPI interface,I2C interface.Block Transfer Engine (BTE) Supports  with 2D£¬Geometry Accelerated Graphics Engine,Support DMA Direct Access FLASH¡£");
	lcd_reg_write(0x21,0x00);//Recovery of register
	lcd_reg_write(0x2F,0x00);//Recovery of register


	LOG("Font Tests Done\n");
	sleep_ms(700);
	sleep_ms(700);
}

static const char * animals[]={
"Aardvark","Albatross","Alligator","Alpaca","Ant","Anteater","Antelope","Ape","Armadillo","Ass/donkey","Baboon","Badger","Barracuda","Bat","Bear ","Beaver","Bee","Bird ","Bison","Boar ","Buffalo","African Butterfly","Camel","Caribou","Cassowary","Cat ","Caterpillar","Chamois","Cheetah","Chicken ","Chimpanzee","Chinchilla","Chough","Coati","Cobra","Cockroach","Cod","Cormorant","Coyote","Crab","Crane","Crocodile","Crow","Curlew","Deer","Dinosaur","Dog","Dogfish","Dolphin","Donkey","Dotterel","Dove","Dragonfly","Duck ","Dugong","Dunlin","Eagle","Echidna","Eel","Eland","Elephant","Elephant seal","Elk","Emu","Falcon","Ferret","Finch","Fish","Flamingo","Fly","Fox","Frog","Gaur","Gazelle","Gerbil","Giant panda","Giraffe","Gnat","Gnu","Goat","Goldfinch","Goosander","Goose","Gorilla","Goshawk","Grasshopper","Grouse","Guanaco","Guinea fowl","Guinea pig","Gull","Hamster","Hare","Hawk","Hedgehog","Heron","Herring","Hippie","Hornet","Horse ","Hummingbird","Hyena","Ibex","Ibis","Jackal","Jaguar","Jay","Jellyfish","Kangaroo","Kinkajou","Koala","Komodo dragon","Kouprey","Kudu","Lapwing","Lark","Lemur","Leopard","Lion","Llama","Lobster","Locust ","Loris","Louse","Lyrebird","Magpie","Mallard","Mammoth","Manatee","Mandrill","Mink","Mole","Mongoose","Monkey","Moose","Mosquito","Mouse","Narwhal","Newt","Nightingale","Octopus","Okapi","Opossum","Ostrich","Otter","Owl","Oyster","Panda","Panther","Parrot","Partridge","Peafowl","Pelican","Penguin","Pheasant","Pig","Pigeon","Polar bear","Pony","Porcupine","Porpoise","Prairie dog","Quail","Quelea","Quetzal","Rabbit ","Raccoon","Ram","Rat","Raven","Red deer","Red panda","Reindeer","Rhinoceros","Rook","Salamander","Salmon","Sand dollar","Sandpiper","Sardine","Sea lion","Sea urchin","Seahorse","Seal","Shark","Sheep","Shrew","Skunk","Sloth","Snail","Snake ","Spider ","Squirrel","Starling","Stegosaurus","Swan","Tapir","Tarsier","Termite","Tiger","Toad","Turkey","Turtle","Vicuña","Wallaby","Walrus","Wasp","Water buffalo","Weasel","Whale","Wolf","Wolverine","Wombat","Wren","Yak","Zebra"};


volatile int8_t previous_mode=0xFF;

void test_bunchoellipses()
{
        INFO("Mode->txt\n");
	lcd_mode_txt();
	lcd_reg_write(RA8875_FNCR0, RA8875_FNCR0_CGROM_INTERNAL | RA8875_FNCR0_ISO8859_1);
	lcd_reg_write(RA8875_FWTSR, RA8875_FWTSR_16 | 0x01);	//Set the characters mode 16 x16 / spacing 1
	
        INFO("Mode->gfx\n");
	lcd_mode_gfx();
                              
        
        INFO("Bk clear\n");
	lcd_text_bk_color1(0x0000);
	lcd_clear();
	lcd_clear_barrier();

	float h = 479.0f;
	float w = 799.0f;
	float vm = 0.2;
	float vx = 1.0;
	float s = 1.0;
	unsigned int period_ms = 100; // new shape every so many ms
	float v;
	int i = 0; 
	
        LOG("Testing Primitives Rendering.\n");

	for (i = 0; i < 200; ++i){


		struct timeval start, end;
		gettimeofday(&start, NULL);

                uint8_t mode = rand() % 6;
		
		switch(mode){
			case 0: 
				INFO("%c",'e');
				lcd_mode_gfx();
				lcd_ellipse(    w *rand()/RAND_MAX, 
						h *rand()/RAND_MAX, 
						1+(w/2-1) * rand()/RAND_MAX,
						1+(h/2-1) * rand()/RAND_MAX,
						//random_color(), 
						HSVtoRGB565(360.0f * rand()/RAND_MAX, s, vm + (vx-vm)*rand()/RAND_MAX),
						//HSVtoRGB565( i % 360, 1, 1.0),
						rand()%2);
				INFO("%c",'.');
				break;
			case 1: 
				INFO("%c",'t');
				lcd_mode_gfx();
				lcd_triangle(   w * rand()/RAND_MAX, 
						h * rand()/RAND_MAX,
						w * rand()/RAND_MAX, 
						h * rand()/RAND_MAX, 
						w * rand()/RAND_MAX, 
						h * rand()/RAND_MAX,
						HSVtoRGB565(360.0f * rand()/RAND_MAX, s, vm + (vx-vm)*rand()/RAND_MAX),
						rand()%2);
				INFO("%c",'.');
				break; 
			case 2:  
				INFO("%c",'r');
				lcd_mode_gfx();
				lcd_rectangle(  w * rand()/RAND_MAX, 
						h * rand()/RAND_MAX,
						w * rand()/RAND_MAX, 
						h * rand()/RAND_MAX, 
						HSVtoRGB565(360.0f * rand()/RAND_MAX, s, vm + (vx-vm)*rand()/RAND_MAX),
						rand()%2);
				INFO("%c",'.');
				break;
			case 3:  
				INFO("%c",'u');
				lcd_mode_gfx();
				lcd_curve_corner(w *rand()/RAND_MAX, 
						h *rand()/RAND_MAX, 
						1+(w/2-1) * rand()/RAND_MAX,
						1+(h/2-1) * rand()/RAND_MAX,
						rand()%4,
						HSVtoRGB565(360.0f * rand()/RAND_MAX, s, vm + (vx-vm)*rand()/RAND_MAX),
						rand()%2);                       
				INFO("%c",'.');
				break;
			case 4: // fonts
			case 5: // fonts

				INFO("%c",'x');
				lcd_mode_txt();

				lcd_text_props(rand() % 4, rand() % 2);
				lcd_font_write_pos( w * rand()/RAND_MAX, h * rand()/RAND_MAX);

				v =  (vx-vm)*rand()/RAND_MAX;
				lcd_text_bk_color1(HSVtoRGB565(360.0f * rand()/RAND_MAX, s, vm + (vx-vm-v)));
				lcd_text_fg_color1(HSVtoRGB565(360.0f * rand()/RAND_MAX, s, vm + v));
				lcd_text_out(animals[rand()%sizeof(animals)/sizeof(animals[0])]);

				INFO("%c",'.');
				break;
		}


		gettimeofday(&end, NULL);
		unsigned int ms_elapsed =  (1000 + (end.tv_usec - start.tv_usec)/1000)%1000;
		if (ms_elapsed < period_ms)
			sleep_ms(period_ms -ms_elapsed);
		else
			WARNING("Last render took %dms\n", ms_elapsed);
	
		previous_mode = mode;
	}

}
