#pragma once

#include <linux/types.h>
#include <stdint.h>

unsigned long 	spi_open(const char *device, uint8_t *mode, uint8_t *bits, uint32_t *speed, uint16_t *delay);
void 		spi_close(unsigned long d);
uint8_t 	spi_tr(unsigned long d, uint8_t v0, uint8_t v1);

uint8_t spi_tr_bulk(unsigned long d, uint8_t *data, uint32_t count);

