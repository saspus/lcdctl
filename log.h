#ifndef LOG_H
#define LOG_H

enum loglevel_e {LOG_ALWAYS, LOG_ERROR, LOG_WARNING, LOG_DEBUG, LOG_TRACE, LOG_INFO};

enum loglevel_e get_log_level(void);
void set_log_level(enum loglevel_e level);

void lclog(enum loglevel_e level, const char * fmt, ...);

#define LOG(fmt, ...)  		lclog(LOG_ALWAYS, fmt, ##__VA_ARGS__)
#define ERROR(fmt, ...)  	lclog(LOG_ERROR, fmt, ##__VA_ARGS__)
#define WARNING(fmt, ...)  	lclog(LOG_WARNING, fmt, ##__VA_ARGS__)
#define DGB(fmt, ...)  		lclog(LOG_DEBUG, fmt, ##__VA_ARGS__)
#define TRACE(fmt, ...)  	lclog(LOG_TRACE, fmt, ##__VA_ARGS__)
#define INFO(fmt, ...)  	lclog(LOG_INFO, fmt, ##__VA_ARGS__)


#endif
