#include <stdio.h>
#include <stdarg.h>
#include "log.h"

static enum loglevel_e g_logLevel=LOG_WARNING;


void set_log_level(enum loglevel_e level)
{
	g_logLevel = level;
	printf("LogLevel set to %d\n", level);
}

enum loglevel_e get_log_level(void)
{
 	return g_logLevel;
}
void lclog(enum loglevel_e level, const char * fmt, ...)
{
        if ( level > get_log_level() )
		return;

	va_list args;

	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);

	fflush(stdout);

}
