#include <linux/types.h>
#include <stdint.h>
#include "lcd.h"
#include "utils.h"   
#include "spi.h"
#include "lcd_regs.h"


static unsigned long g_spi_device;

void lcd_cmd_write(uint8_t cmd)
{
	spi_tr(g_spi_device, RA8875_CMDWRITE, cmd);
}

void lcd_dat_write(uint8_t data)
{
	spi_tr(g_spi_device, RA8875_DATAWRITE, data);
}
void lcd_bulk_write(uint8_t*data, uint32_t count)
{

	spi_tr_bulk(g_spi_device, data,  count);
}


uint8_t lcd_dat_read(void)
{
	return spi_tr(g_spi_device, RA8875_DATAREAD, 0x00);
}

uint8_t lcd_reg_read(uint8_t regnum)
{
	lcd_cmd_write(regnum);
	return lcd_dat_read();
}

void lcd_reg_write(uint8_t regnum, uint8_t data)
{
	lcd_cmd_write(regnum);
	lcd_dat_write(data);
}

uint8_t lcd_status_read(void)
{
	return spi_tr(g_spi_device, RA8875_CMDREAD, 0x00);
}


void lcd_pll_init()
{
	lcd_reg_write(RA8875_PLLC1, 0x0b);
	sleep_ms(1);     
	lcd_reg_write(RA8875_PLLC2, RA8875_PLLC2_DIV4);
	sleep_ms(1);
}	  

void lcd_init(unsigned long spi_device)
{ 	
        g_spi_device = spi_device;

	lcd_pll_init();
	
	lcd_reg_write(RA8875_SYSR, RA8875_SYSR_16BPP | RA8875_SYSR_MCU8);


	/* Timing values */
	uint16_t width=800;
	uint16_t height=480;
	uint8_t pixclk;
	uint8_t hsync_start;
	uint8_t hsync_pw;
	uint8_t hsync_finetune;
	uint8_t hsync_nondisp;
	uint8_t vsync_pw; 
	uint16_t vsync_nondisp;
	uint16_t vsync_start;

	pixclk          = RA8875_PCSR_PDATL | RA8875_PCSR_2CLK;
	hsync_nondisp   = 26;
	hsync_start     = 32;
	hsync_pw        = 96;
	hsync_finetune  = 0;
	vsync_nondisp   = 32;
	vsync_start     = 23;
	vsync_pw        = 2;

	lcd_reg_write(RA8875_PCSR, pixclk);
	sleep_ms(1);
	
	/* Horizontal settings registers */
	lcd_reg_write(RA8875_HDWR, (width / 8) - 1);                          // H width: (HDWR + 1) * 8 = 480
	lcd_reg_write(RA8875_HNDFTR, RA8875_HNDFTR_DE_HIGH + hsync_finetune);
	lcd_reg_write(RA8875_HNDR, (hsync_nondisp - hsync_finetune - 2)/8);    // H non-display: HNDR * 8 + HNDFTR + 2 = 10
	lcd_reg_write(RA8875_HSTR, hsync_start/8 - 1);                         // Hsync start: (HSTR + 1)*8 
	lcd_reg_write(RA8875_HPWR, RA8875_HPWR_LOW + (hsync_pw/8 - 1));        // HSync pulse width = (HPWR+1) * 8

	/* Vertical settings registers */
	lcd_reg_write(RA8875_VDHR0, (uint16_t)(height - 1) & 0xFF);
	lcd_reg_write(RA8875_VDHR1, (uint16_t)(height - 1) >> 8);
	lcd_reg_write(RA8875_VNDR0, vsync_nondisp-1);                          // V non-display period = VNDR + 1
	lcd_reg_write(RA8875_VNDR1, vsync_nondisp >> 8);
	lcd_reg_write(RA8875_VSTR0, vsync_start-1);                            // Vsync start position = VSTR + 1
	lcd_reg_write(RA8875_VSTR1, vsync_start >> 8);
	lcd_reg_write(RA8875_VPWR, RA8875_VPWR_LOW + vsync_pw - 1);            // Vsync pulse width = VPWR + 1

	lcd_active_window(0, 0, width-1, height-1);
       
	lcd_scroll_offset(0, 0);
}

void lcd_display_on(bool on)
{         
	if (on) 
		lcd_reg_write(RA8875_PWRR, RA8875_PWRR_NORMAL | RA8875_PWRR_DISPON);
	else
		lcd_reg_write(RA8875_PWRR, RA8875_PWRR_NORMAL | RA8875_PWRR_DISPOFF);
}

void lcd_bklight_on(bool on)
{
	if (on)
		lcd_reg_write(RA8875_P1CR, RA8875_P1CR_ENABLE | 1);
	else	
		lcd_reg_write(RA8875_P1CR, RA8875_P1CR_DISABLE | 1);
}

void lcd_bklight_level(uint8_t v)
{
	lcd_reg_write(RA8875_P1DCR, v);
}

void lcd_wait_idle(void)
{
	while((lcd_status_read()&0x80)==0x80);
}

void lcd_wait_bte_idle(void)
{
	while((lcd_status_read()&0x40)==0x40);
}

void lcd_wait_dma_idle(void)
{
	lcd_cmd_write(0xBF);
	while((lcd_dat_read() & 0x01) == 0x01);
	
/*	uint8_t temp; 	
	do
	{
		lcd_cmd_write(0xBF);
		temp =lcd_dat_read();
	}while((temp&0x01)==0x01);   
        */
}

bool lcd_poll_wait(uint8_t reg, uint8_t bits)
{
	lcd_cmd_write(reg);

	while((lcd_dat_read() & bits) == bits);

	return true;
}

void lcd_clear(void)
{
	lcd_reg_write(RA8875_MCLR, RA8875_MCLR_START | RA8875_MCLR_FULL);
}

bool lcd_clear_barrier(void){
	return lcd_poll_wait(RA8875_MCLR, RA8875_MCLR_READSTATUS);
}

enum lcd_mode {modeUnknown, modeGFX, modeTXT};

	
static enum lcd_mode _mode = modeUnknown;
void lcd_mode_gfx()
{
	
	if (_mode != modeGFX)
	{
		lcd_cmd_write(RA8875_MWCR0);
		lcd_dat_write( lcd_dat_read() & (~RA8875_MWCR0_TXTMODE) );
                _mode = modeGFX;
	}
}
void lcd_mode_txt()
{
	if (_mode != modeTXT)
	{
		lcd_cmd_write(RA8875_MWCR0);
		lcd_dat_write(RA8875_MWCR0_TXTMODE | lcd_dat_read());
                _mode = modeTXT;
	}
}

void lcd_font_write_pos(uint16_t X,uint16_t Y)
{
	lcd_reg_write(0x2A, X & 0xFF);
	lcd_reg_write(0x2B, (X >> 8) && 0xFF);
	lcd_reg_write(0x2C, Y & 0xFF);
	lcd_reg_write(0x2D, (Y >> 8) && 0xFF);
}

void lcd_text_props(uint8_t scale, bool tranparent)
{
	if (scale > 3) scale = 3;
	lcd_cmd_write(RA8875_FNCR1);
        uint8_t val = lcd_dat_read();
	val &= ~(0xf);
	val |= scale | (scale << 2);

	if (tranparent)
		val |= RA8875_FNCR1_TRANSPARENT;
	else
		val &= ~ RA8875_FNCR1_TRANSPARENT;

	lcd_dat_write(val);
}
void lcd_text_out(const char *str)
{   
	lcd_cmd_write(RA8875_MRWC);
	while(*str != '\0')
	{
		lcd_dat_write((uint8_t)*str++);
		lcd_wait_idle();		
	} 
}

void lcd_active_window(uint16_t HS, uint16_t VS, uint16_t HE, uint16_t VE)
{
	lcd_reg_write(RA8875_HSAW0, (HS >> 0) & 0xFF); 
	lcd_reg_write(RA8875_HSAW1, (HS >> 8) & 0xFF); 
                                                       
	lcd_reg_write(RA8875_HEAW0, (HE >> 0) & 0xFF); 
	lcd_reg_write(RA8875_HEAW1, (HE >> 8) & 0xFF); 
                                                       
	lcd_reg_write(RA8875_VSAW0, (VS >> 0) & 0xFF); 
	lcd_reg_write(RA8875_VSAW1, (VS >> 8) & 0xFF); 
	                                               
	lcd_reg_write(RA8875_VEAW0, (VE >> 0) & 0xFF); 
	lcd_reg_write(RA8875_VEAW1, (VE >> 8) & 0xFF); 
}

void lcd_scroll_window(uint16_t HS, uint16_t VS, uint16_t HE, uint16_t VE)
{
     lcd_reg_write(0x38, (HS >> 0) & 0xFF);
     lcd_reg_write(0x39, (HS >> 8) & 0xFF);

     lcd_reg_write(0x3c, (HE >> 0) & 0xFF);
     lcd_reg_write(0x3d, (HE >> 8) & 0xFF);
                            
     lcd_reg_write(0x3a, (VS >> 0) & 0xFF);
     lcd_reg_write(0x3b, (VS >> 8) & 0xFF);

     lcd_reg_write(0x3e, (VE >> 0) & 0xFF);
     lcd_reg_write(0x3f, (VE >> 8) & 0xFF);
}

void lcd_scroll_offset(uint16_t X, uint16_t Y)
{
     lcd_reg_write(0x24, (X >> 0) & 0xFF);
     lcd_reg_write(0x25, (X >> 8) & 0xFF);

     lcd_reg_write(0x26, (Y >> 0) & 0xFF);
     lcd_reg_write(0x27, (Y >> 8) & 0xFF);
}

void lcd_ellipse(int16_t xc, int16_t yc, int16_t longaxis, int16_t shortaxis, uint16_t color, bool filled) 
{
        lcd_reg_write(0xA5, xc & 0xFF);
 	lcd_reg_write(0xA6, (xc >> 8) & 0xFF);
        lcd_reg_write(0xA7, yc & 0xFF);
 	lcd_reg_write(0xA8, (yc >> 8) & 0xFF);
	

        lcd_reg_write(0xA1, longaxis & 0xFF);
 	lcd_reg_write(0xA2, (longaxis >> 8) & 0xFF);
        lcd_reg_write(0xA3, shortaxis & 0xFF);
 	lcd_reg_write(0xA4, (shortaxis >> 8) & 0xFF);
	
	lcd_reg_write(0x63, (color & 0xF800) >> 11);
	lcd_reg_write(0x64, (color & 0x07e0) >> 5);
	lcd_reg_write(0x65, (color & 0x001f));

	lcd_reg_write(0xA0, filled ? 0xC0 : 0x80);
	lcd_poll_wait(0xA0, 0x80);
}


void lcd_triangle( int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color, bool filled)
{

        lcd_reg_write(0x91, x0 & 0xFF);
 	lcd_reg_write(0x92, (x0 >> 8) & 0xFF);
        lcd_reg_write(0x93, y0 & 0xFF);
 	lcd_reg_write(0x94, (y0 >> 8) & 0xFF);
        lcd_reg_write(0x95, x1 & 0xFF);
 	lcd_reg_write(0x96, (x1 >> 8) & 0xFF);
        lcd_reg_write(0x97, y1 & 0xFF);
 	lcd_reg_write(0x98, (y1 >> 8) & 0xFF);
        
        lcd_reg_write(0xA9, x2 & 0xFF);
 	lcd_reg_write(0xAA, (x2 >> 8) & 0xFF);
        lcd_reg_write(0xAB, y2 & 0xFF);
 	lcd_reg_write(0xAC, (y2 >> 8) & 0xFF);


	lcd_reg_write(0x63, (color & 0xF800) >> 11);
	lcd_reg_write(0x64, (color & 0x07e0) >> 5);
	lcd_reg_write(0x65, (color & 0x001f));

	lcd_reg_write(0x90, filled ? 0xA1 : 0x81);

 	lcd_poll_wait(0x90, 0x80);
}

void lcd_rectangle( int16_t x, int16_t y, int16_t w, int16_t h, int16_t color, bool filled)
{

        lcd_reg_write(0x91, x & 0xFF);
 	lcd_reg_write(0x92, (x >> 8) & 0xFF);
        lcd_reg_write(0x93, y & 0xFF);
 	lcd_reg_write(0x94, (y >> 8) & 0xFF);
        lcd_reg_write(0x95, w & 0xFF);
 	lcd_reg_write(0x96, (w >> 8) & 0xFF);
        lcd_reg_write(0x97, h & 0xFF);
 	lcd_reg_write(0x98, (h >> 8) & 0xFF);
        

	lcd_reg_write(0x63, (color & 0xF800) >> 11);
	lcd_reg_write(0x64, (color & 0x07e0) >> 5);
	lcd_reg_write(0x65, (color & 0x001f));

	lcd_reg_write(0x90, filled ? 0xB0 : 0x90);

 	lcd_poll_wait(0x90, 0x80);
}

void  lcd_curve_corner(int16_t xc, int16_t yc, int16_t longaxis, int16_t shortaxis, uint8_t icorner, uint16_t color, bool filled)
{
        lcd_reg_write(0xA5, xc & 0xFF);
 	lcd_reg_write(0xA6, (xc >> 8) & 0xFF);
        lcd_reg_write(0xA7, yc & 0xFF);
 	lcd_reg_write(0xA8, (yc >> 8) & 0xFF);
	

        lcd_reg_write(0xA1, longaxis & 0xFF);
 	lcd_reg_write(0xA2, (longaxis >> 8) & 0xFF);
        lcd_reg_write(0xA3, shortaxis & 0xFF);
 	lcd_reg_write(0xA4, (shortaxis >> 8) & 0xFF);
	
	lcd_reg_write(0x63, (color & 0xF800) >> 11);
	lcd_reg_write(0x64, (color & 0x07e0) >> 5);
	lcd_reg_write(0x65, (color & 0x001f));
	

	lcd_reg_write(0xA0, (filled ? 0xD0 : 0x90)|(icorner & 0x03));
	lcd_poll_wait(0xA0, 0x80);

}
//-----------------------------------------------


void lcd_text_bk_color1(uint16_t b_color)
{

	lcd_cmd_write(0x60);//BGCR0
	lcd_dat_write((uint8_t)(b_color>>11));

	lcd_cmd_write(0x61);//BGCR0
	lcd_dat_write((uint8_t)(b_color>>5));

	lcd_cmd_write(0x62);//BGCR0
	lcd_dat_write((uint8_t)(b_color));
} 

void lcd_text_bk_color(uint8_t setR, uint8_t setG, uint8_t setB)
{
	lcd_cmd_write(0x60);//BGCR0
	lcd_dat_write(setR);

	lcd_cmd_write(0x61);//BGCR0
	lcd_dat_write(setG);

	lcd_cmd_write(0x62);//BGCR0
	lcd_dat_write(setB);
} 

void lcd_text_fg_color1(uint16_t b_color)
{

	lcd_cmd_write(0x63);//BGCR0
	lcd_dat_write((uint8_t)(b_color>>11));

	lcd_cmd_write(0x64);//BGCR0
	lcd_dat_write((uint8_t)(b_color>>5));

	lcd_cmd_write(0x65);//BGCR0
	lcd_dat_write((uint8_t)(b_color));
} 

void lcd_text_fg_color(uint8_t setR, uint8_t setG, uint8_t setB)
{	    
	lcd_cmd_write(0x63);//BGCR0
	lcd_dat_write(setR);

	lcd_cmd_write(0x64);//BGCR0
	lcd_dat_write(setG);

	lcd_cmd_write(0x65);//BGCR0
	lcd_dat_write(setB);
}

